package mono;
public class MonoPackageManager_Resources {
	public static String[] Assemblies = new String[]{
		/* We need to ensure that "calculaor.dll" comes first in this list. */
		"calculaor.dll",
		"Xamarin.Android.Support.Compat.dll",
		"Xamarin.Essentials.dll",
	};
	public static String[] Dependencies = new String[]{
	};
}
