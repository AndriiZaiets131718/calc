﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using OpenQA.Selenium;
using NUnit.Framework;

namespace ClassLibrary1
{
    [TestFixture]
    public class Class1

    {   [Test]
        public void test()
        {
            AppiumOptions caps = new AppiumOptions();

            // Set your BrowserStack access credentials
            //caps.AddAdditionalCapability("browserstack.user", "bsuser_OfxlF1");
            //caps.AddAdditionalCapability("browserstack.key", "fb2rDESsxypp1raZvWm3");

            // Set URL of the application under test
            caps.AddAdditionalCapability("app", "@путь к apk");

            // Specify device and os_version
            caps.AddAdditionalCapability("device", "Google Pixel 3");
            caps.AddAdditionalCapability("os_version", "9.0");

            // Specify the platform name
            caps.PlatformName = "Android";

            // Set other BrowserStack capabilities
            caps.AddAdditionalCapability("platformName", "Android");
            caps.AddAdditionalCapability("platformVersion", "9.0");
            caps.AddAdditionalCapability("automationName", "UIAutomator2");
            caps.AddAdditionalCapability("deviceName", "Pixel 2");
            caps.AddAdditionalCapability("app", "C:\\Users\\38067\\Desktop\\apk\\com.companyname.calculaor.apk");


           // Initialize the remote Webdriver using BrowserStack remote URL
           // and desired capabilities defined above
           AndroidDriver <AndroidElement> driver = new AndroidDriver<AndroidElement>(
                    new Uri("http://localhost:4723/wd/hub"), caps);

            // Write your custom code here
            AndroidElement twoButton = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[12]"));
            twoButton.Click();
            AndroidElement plusOperation = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[14]"));
            plusOperation.Click();
            twoButton.Click();
            AndroidElement equalOperation = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[17]"));
            equalOperation.Click();
            AndroidElement textField = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.TextView"));
            string actual = textField.Text;
            Assert.AreEqual("4", actual);

            Console.WriteLine(actual);

            // Invoke driver.quit() after the test is done to indicate that the test is completed.
            driver.Quit();

            

        }

    }
}
